package main

import (
	log "github.com/Sirupsen/logrus"
	"uber.com/grumpy/commands"
)

func main() {
	if err := commands.GrumpyCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
