FROM golang:1.9-alpine AS builder

RUN apk --no-cache add git
RUN apk add --update alpine-sdk

ENV D=/go/src/uber.com
RUN go get -u github.com/golang/dep/...
#ADD ./api/Gopkg.* $D/api/
#RUN cd $D/api && dep ensure -v --vendor-only
# build
ADD . $D/grumpy
RUN cd $D/grumpy && go get 
RUN cd $D/grumpy && go build -o /bin/grumpy main.go
RUN cd $D/grumpy && /bin/grumpy load && cp grumpy.db /

FROM alpine
RUN apk --no-cache add ca-certificates
WORKDIR /app/server/
COPY --from=builder /grumpy.db /app/server/grumpy.db
COPY --from=builder /bin/grumpy /bin/grumpy
EXPOSE 8000
CMD ["/bin/grumpy", "-H", "0.0.0.0", "-p", "8000", "serve"]
