package ingest

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/blevesearch/bleve"
	"github.com/davecgh/go-spew/spew"
	"gopkg.in/yaml.v2"
	//	"github.com/pksunkara/pygments"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/sanity-io/litter"
	"gopkg.in/russross/blackfriday.v2"
	// my home emacs config required blackfriday or it would delete this import even
	// if it was used
	//	blackfriday "gopkg.in/russross/blackfriday.v2"
	metadataHandler "uber.com/grumpy/metadata"
	"uber.com/grumpy/models"
	"uber.com/grumpy/utils"
)

func dumpHold() {
	spew.Dump(42)
	litter.Dump(3)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type sectionHeader struct {
	Level  int
	Anchor string
	Tag    string
	Title  string
}

type fragmentContent struct {
	Header  sectionHeader
	Content []byte
}

type ruleContent struct {
	Header  sectionHeader
	Content []fragmentContent
}

type sectionContent struct {
	Header sectionHeader
	Rules  []ruleContent
}

type documentContent struct {
	Sections []sectionContent
}

var anchorRe = regexp.MustCompile(`<a name="([^"]*)">`)

// TODO(mav) allow arbitry number of spaces before actual title
var tagRe = regexp.MustCompile(`^([^:]*): (.*)$`)

func renderHeading(header sectionHeader, asText bool) string {
	if asText {
		return header.Tag + ": " + header.Title
	} else {
		result := "\n" + strings.Repeat("#", header.Level) + " "
		if header.Anchor != "" {
			result += "<a name=\"" + header.Anchor + "\"></a>"
		}
		if header.Tag != "" {
			result += header.Tag + ":"
		}
		result += header.Title + "\n"
		return result
	}
}

func getHeading(node *blackfriday.Node) sectionHeader {
	var h sectionHeader
	title := ""
	h.Level = node.HeadingData.Level
	for c := node.FirstChild; c != nil; c = c.Next {
		switch c.Type {
		case blackfriday.Text:
			title += string(c.Literal)
		case blackfriday.Code:
			title += "`" + string(c.Literal) + "`"
		case blackfriday.Link:
			title += "[" + string(c.FirstChild.Literal) + "](" + string(c.LinkData.Destination) + ")"
		case blackfriday.HTMLSpan:
			if string(c.Literal) != "</a>" {
				// TODO(mav) add error checking in case we do not find an anchor
				h.Anchor = string(anchorRe.FindAllSubmatch(c.Literal, -1)[0][1])
			}
		default:
			fmt.Printf("TAG IS %v\n", c.Type)
			log.Fatalf("Unhandled fragment type '%v' |%v| in header\n", c.Type, string(c.Literal))
		}
	}

	if m := tagRe.FindAllStringSubmatch(title, -1); m != nil {
		//if m != nil {
		h.Tag = m[0][1]
		h.Title = m[0][2]
	} else {
		h.Title = title
	}
	return h
}

type ruleRenderer struct {
	Content         documentContent
	currentSection  *sectionContent
	currentRule     *ruleContent
	currentFragment *fragmentContent
	content         []byte
	CurrentListItem int
	DB              *gorm.DB
	index           bleve.Index
	log             *log.Logger
}

func skipSection(header sectionHeader) bool {
	if header.Tag == "" {
		return true
	}
	m, ok := metadataHandler.GetSectionMetadata(header.Tag)
	//	fmt.Printf(">>> %s %v %s\n", header.Tag, ok, m.Action)
	if !ok || m.Action != "keep" {
		return true
	}

	return false
}

func (r *ruleRenderer) dumpDocument() {
	fmt.Println("\nDocument\n=====================\n")

	for _, section := range r.Content.Sections {
		fmt.Println(renderHeading(section.Header, true))
		for _, rule := range section.Rules {
			fmt.Println(renderHeading(rule.Header, true))
			for _, fragment := range rule.Content {
				fmt.Println("=================================")
				fmt.Println(string(fragment.Content))
			}
		}
	}
}

func (r *ruleRenderer) indexDatabase() {
	for _, section := range r.Content.Sections {
		for _, rule := range section.Rules {
			content := ""
			tag := rule.Header.Tag
			for _, fragment := range rule.Content {
				if fragment.Header.Title != "Example" {
					content += string(fragment.Content)
				}
			}
			r.index.Index(tag, content)
		}
	}
}

func (r *ruleRenderer) seedDatabase() {
	r.lint()
	r.indexDatabase()
	for _, section := range r.Content.Sections {
		sm, _ := metadataHandler.GetSectionMetadata(section.Header.Tag)
		var rules []models.Rule

		for _, rule := range section.Rules {
			incomplete := false
			rm, _ := metadataHandler.GetRuleMetadata(rule.Header.Tag)
			fragmentPosition := 1
			var fragments []models.Fragment
			var tags []models.Tag
			tags = tags[:0]

			for _, fragment := range rule.Content {
				if incomplete == false && bytes.Contains(fragment.Content, []byte("???")) {
					incomplete = true
					tag := models.Tag{}
					r.DB.Debug().Where(models.Tag{Label: "incomplete"}).FirstOrCreate(&tag)
					tags = append(tags, tag)
				}
				fragments = append(fragments, models.Fragment{
					Title:    fragment.Header.Title,
					Content:  string(fragment.Content),
					Position: fragmentPosition,
				})
				fragmentPosition += 1
			}
			for _, tagLabel := range sm.Tags {
				tag := models.Tag{}
				r.DB.Debug().Where(models.Tag{Label: tagLabel}).FirstOrCreate(&tag)
				tags = append(tags, tag)
			}
			rules = append(rules, models.Rule{
				Anchor:    rule.Header.Anchor,
				Tag:       rule.Header.Tag,
				Title:     rule.Header.Title,
				Fragments: fragments,
				Position:  rm.Position,
				Tags:      tags,
			})
		}

		sectionEntry := models.Section{
			Anchor:   section.Header.Anchor,
			Tag:      section.Header.Tag,
			Title:    section.Header.Title,
			Rules:    rules,
			Position: sm.Position,
		}

		r.DB.Create(&sectionEntry)
	}
}

func (r *ruleRenderer) lint() {
	for _, section := range r.Content.Sections {
		sm, ok := metadataHandler.GetSectionMetadata(section.Header.Tag)
		if ok {
			fmt.Printf("%d> %s\n", sm.Position, sm.Action)

		} else {
			fmt.Printf("Unknown section: %s:%s\n", section.Header.Tag, section.Header.Title)
		}
	}
}

func escapeTitle(t string) string {
	// We add a space or we're not marshaling properly
	// TODO(mav) figure this out
	m, _ := yaml.Marshal(" " + t)
	s := "'" + strings.TrimPrefix(string(m), "' ")
	return strings.Trim(string(s), " \n")
}

func (r *ruleRenderer) generateMetadata(metadataFile string) {
	file, err := os.Create(metadataFile)
	check(err)
	defer file.Close()

	header := `
# This file describes the rules in the ATG style guide without causing spurious diffs in the
# main text. The rules themselves are derived from the C++ Core guidelines, with ATG specific
# additions and modifications.
#
# The [sections] part describes sections as a whole. It allows for entire sections to be skipped.
# The order of sections in this file is the one grumpy uses for displaying list of sections.
# The following fields are required:
#   section: the tag for the section
#   action: whether to keep/drop/add the section
# The following fields are optional:
#   title: the full title of the section
#   tags: tags that apply to all rules in the section
#   reason: the reason for the action
#
# The [rules] part is similar, but each entry describes a single rule.
# In addition, an optional field 'checker' lists the checkers that are able to catch something the rules
# talks about.
`
	fmt.Fprintf(file, "%s\n\n", header)

	fmt.Fprintf(file, "sections:\n")
	for _, section := range r.Content.Sections {
		//		title := escapeTitle(section.Header.Title)
		//		title, _ := yaml.Marshal(" " + section.Header.Title)
		fmt.Fprintf(file, "  - section: %s\n", section.Header.Tag)
		//		fmt.Fprintf(file, "    title: %s\n", strings.Trim(string(title), " \n"))
		fmt.Fprintf(file, "    title: %s\n", escapeTitle(section.Header.Title))
		fmt.Fprintf(file, "    action: keep\n")
		fmt.Fprintf(file, "    reason: it applies to ATG\n")
		fmt.Fprintf(file, "    tags: []\n")

	}

	fmt.Fprintf(file, "\n\n # Rule metadata \n#%s\n\n", strings.Repeat("-", 60))
	fmt.Fprintf(file, "rules:\n")
	for _, section := range r.Content.Sections {
		fmt.Fprintf(file, "\n# Section %s\n", section.Header.Title)
		for _, rule := range section.Rules {
			fmt.Fprintf(file, "  - rule: %s\n", rule.Header.Tag)
			//			title := escapeTitle(rule.Header.Title)
			//			title, _ := yaml.Marshal(" " + rule.Header.Title)
			//			fmt.Fprintf(file, "    title: %s\n", strings.Trim(string(title), " \n"))
			fmt.Fprintf(file, "    title: %s\n", escapeTitle(rule.Header.Title))
			fmt.Fprintf(file, "    action: keep\n")
			fmt.Fprintf(file, "    tags: []\n")
		}
	}
}

func (r *ruleRenderer) enterSection(header sectionHeader) {
	r.exitCurrentSection()
	if skipSection(header) {
		//		r.log.Print("{SKIPPED section} " + renderHeading(header, true))
		return
	}

	//	r.log.Print("{section} " + renderHeading(header, true))
	r.currentSection = &sectionContent{Header: header}
}

func (r *ruleRenderer) enterRule(header sectionHeader) {
	if r.currentSection != nil {
		r.exitRule()
		//		r.log.Print(renderHeading(header, true))
		r.currentRule = &ruleContent{Header: header}
	}
}

func (r *ruleRenderer) enterFragment(header sectionHeader) {
	r.exitFragment()
	if r.currentSection != nil && r.currentRule != nil {
		//		r.log.Print(renderHeading(header, true))
		r.currentFragment = &fragmentContent{Header: header}
		return
	}
}

func (r *ruleRenderer) exitFragment() {
	if r.currentFragment != nil {
		r.currentFragment.Content = append(r.currentFragment.Content, r.content...)
		r.currentRule.Content = append(r.currentRule.Content, *r.currentFragment)
		r.content = nil
	}
	r.currentFragment = nil
}

func (r *ruleRenderer) exitRule() {
	r.exitFragment()
	if r.currentRule != nil {
		r.currentSection.Rules = append(r.currentSection.Rules, *r.currentRule)
		r.currentRule = nil
	}
}

func (r *ruleRenderer) exitCurrentSection() {
	r.exitRule()
	if r.currentSection != nil {
		r.Content.Sections = append(r.Content.Sections, *r.currentSection)
		r.currentSection = nil
	}
}

func (r *ruleRenderer) getContent() []byte {
	return r.content
}

func (r *ruleRenderer) clearContent() {
	r.content = nil
}

func (r *ruleRenderer) Write(buf []byte) {
	//	r.content = append(r.content, buf...)
	if r.currentFragment != nil {
		r.currentFragment.Content = append(r.currentFragment.Content, buf...)
	} else {
		//		r.log.Print("Nowhere to put this guy: ")
	}
}

func (r *ruleRenderer) handleHeading(node *blackfriday.Node) {
	header := getHeading(node)
	//	fmt.Printf("Entering [%d] %v[%v] ->\t %v\n", header.Level, header.Anchor, header.Tag, header.Title)
	// TODO(mav) only to minimize diffs while debugging
	//r.Write([]byte(renderHeading(header)))
	switch header.Level {
	case 1:
		r.enterSection(header)
	case 2:
		// check we don't get this inside interesting sections
	case 3:
		r.enterRule(header)
	case 4:
		// pretend it is 5, but fix upstream
		r.enterFragment(header)
	case 5:
		r.enterFragment(header)
	default:
		panic(fmt.Sprintf("Unexpected header level: %d title: %s\n",
			header.Level, header.Title))
	}
}

func (r *ruleRenderer) exitSection(node *blackfriday.Node) {
	//header := getHeading(node)
	//	fmt.Printf("Exiting [%d] %v[%v] ->\t %v\n", header.Level, header.Anchor, header.Tag, header.Title)
}

func renderChildren(node *blackfriday.Node) []byte {
	renderer := ruleRenderer{}

	for c := node.FirstChild; c != nil; c = c.Next {
		c.Walk(func(node *blackfriday.Node, entering bool) blackfriday.WalkStatus {
			return renderer.renderNode(nil, node, entering)
		})
	}

	// node.FirstChild.Walk(func(node *blackfriday.Node, entering bool) blackfriday.WalkStatus {
	// 	return renderer.renderNode(nil, node, entering)
	// })

	return renderer.getContent()
}

func addPrefix(text string, prefix string) string {
	prefixWithNewline := "\n" + prefix
	content := strings.Replace(strings.Trim(text, "\n"), "\n", prefixWithNewline, -1)
	content = strings.Replace(content, prefixWithNewline+"\n", "\n\n", -1)
	return content + "\n"

}

func indent(buffer []byte, spaceCount int) string {
	return addPrefix(string(buffer), strings.Repeat(" ", spaceCount))

	// prefix := "\n" + strings.Repeat(" ", spaceCount)
	// content := strings.Replace(strings.Trim(string(buffer), "\n"), "\n", prefix, -1)
	// content = strings.Replace(content, prefix+"\n", "\n\n", -1)
	// return content + "\n"
}

func escapeBrackets(text []byte) string {
	return strings.Replace(strings.Replace(string(text), "[", "\\[", -1),
		"]", "\\]", -1)
}
func (r *ruleRenderer) renderNode(w io.Writer, node *blackfriday.Node, entering bool) blackfriday.WalkStatus {
	switch node.Type {
	case blackfriday.Document:

	case blackfriday.Heading:
		r.handleHeading(node)

		// if entering {
		// 	r.clearContent()
		// } else {
		// 	fmt.Println("--------------------------------------")
		// 	fmt.Println(string(r.getContent()))
		// 	fmt.Println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		//}
		return blackfriday.SkipChildren

	case blackfriday.Image:
		r.Write([]byte(fmt.Sprintf("![%s](%s",
			renderChildren(node), node.LinkData.Destination)))
		if node.LinkData.Title != nil {
			r.Write([]byte(fmt.Sprintf(" \"%s\"", node.LinkData.Title)))
		}
		r.Write([]byte(")"))
		return blackfriday.SkipChildren

	case blackfriday.Link:
		r.Write([]byte(fmt.Sprintf("[%s](%s",
			escapeBrackets(renderChildren(node)), node.LinkData.Destination)))
		if node.LinkData.Title != nil {
			r.Write([]byte(fmt.Sprintf(" \"%s\"", node.LinkData.Title)))
		}
		r.Write([]byte(")"))
		return blackfriday.SkipChildren

	case blackfriday.Text:
		r.Write([]byte(node.Literal))

	case blackfriday.Paragraph:
		r.Write([]byte("\n"))

	case blackfriday.Emph:
		r.Write([]byte("_"))

	case blackfriday.Strong:
		r.Write([]byte("**"))

	case blackfriday.Code:
		r.Write([]byte("`"))
		r.Write([]byte(node.Literal))
		r.Write([]byte("`"))

	case blackfriday.List:
		if node.ListFlags&blackfriday.ListTypeOrdered != 0 {
			r.CurrentListItem = 1
		} else {
			r.CurrentListItem = -1
		}
		if node.ListFlags&blackfriday.ListTypeDefinition != 0 {
			panic("We don't handle definition lists yet!")
		}
		if node.IsFootnotesList {
			panic("We don't handle footnote lists yet!")
		}
		if entering {
			r.Write([]byte("\n"))
		}

	case blackfriday.Item:
		itemHeader := "*"
		if r.CurrentListItem > 0 {
			itemHeader = fmt.Sprintf("%d.", r.CurrentListItem)
			r.CurrentListItem += 1
		}
		r.Write([]byte(fmt.Sprintf("%s %s", itemHeader, indent(renderChildren(node), 2))))

		//		r.Write([]byte(fmt.Sprintf("* %s",
		//			strings.TrimLeft(string(renderChildren(node)), "\n"))))

		return blackfriday.SkipChildren

	case blackfriday.CodeBlock:
		r.Write([]byte("\n    "))
		r.Write([]byte(indent(node.Literal, 4)))

		// r.Write([]byte("\n    "))
		// code := strings.Replace(strings.TrimRight(string(node.Literal), "\n"), "\n", "\n    ", -1)
		// r.Write([]byte(code))
		// r.Write([]byte("\n"))

	case blackfriday.HTMLSpan:
		r.Write([]byte(node.Literal))

	case blackfriday.Hardbreak:
		r.Write([]byte("\n"))

	case blackfriday.BlockQuote:
		r.Write([]byte(addPrefix(string(node.Literal), "> ")))
		r.Write(node.Literal)

	// case blackfriday.Softbreak:
	// case blackfriday.Del:
	// case blackfriday.HTMLSpan:
	// case blackfriday.Link:
	// case blackfriday.Code:
	// case blackfriday.BlockQuote:
	// case blackfriday.HTMLBlock:
	// case blackfriday.HorizontalRule:
	// case blackfriday.List:
	// case blackfriday.Item:
	//		_, err := r.Write(node.Literal)
	//		check(err)

	default:
		panic("Unknown node type " + node.Type.String())
	}

	return blackfriday.GoToNext
}

func ClearDatabase(database string) {
	db, err := gorm.Open("sqlite3", database)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	db.DropTableIfExists(&models.Rule{}, &models.Section{}, &models.Fragment{}, &models.Tag{})

}

// Populate rule database from file
func RulesIngest(guidelinesFile string, database string, metadata string) error {

	log := log.New(os.Stdout, "[grumpy] ", 0)
	log.Printf("Ingesting rules from %s into %s", guidelinesFile, database)
	log.Printf("control metadata in  %s", metadata)

	db, err := gorm.Open("sqlite3", database)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	db.AutoMigrate(&models.Rule{}, &models.Section{}, &models.Fragment{}, &models.Tag{})

	input, err := ioutil.ReadFile(guidelinesFile)
	check(err)

	bleveIndex := "grumpy.bleve"
	index, err := bleve.Open(bleveIndex)
	if err != nil {
		mapping := bleve.NewIndexMapping()
		index, err = bleve.New(bleveIndex, mapping)
		if err != nil {
			fmt.Println(err)
			return err
		}
	}

	renderer := ruleRenderer{DB: db, log: log, index: index}

	if !utils.FileExists(metadata) {
		renderer.generateMetadata(metadata)
	}
	metadataHandler.GetMetadata(metadata)

	ast := blackfriday.New().Parse(input)
	ast.Walk(func(node *blackfriday.Node, entering bool) blackfriday.WalkStatus {
		return renderer.renderNode(nil, node, entering)
	})

	renderer.seedDatabase()

	return nil
}
