package metadata

import (
	_ "fmt"
	_ "github.com/davecgh/go-spew/spew"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type SectionMetadata struct {
	Section string
	Action  string
	Reason  string
	Tags    []string

	Position int
}

type RuleMetadata struct {
	Rule     string
	Title    string
	Action   string
	Tags     []string
	Checkers []string

	Position int
}

type Metadata struct {
	Sections []SectionMetadata
	Rules    []RuleMetadata
}

var (
	sectionInfo map[string]SectionMetadata
	ruleInfo    map[string]RuleMetadata

	metadata Metadata
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func GetRuleMetadata(rule string) (RuleMetadata, bool) {
	m, ok := ruleInfo[rule]
	return m, ok
}

func GetSectionMetadata(section string) (SectionMetadata, bool) {
	m, ok := sectionInfo[section]
	//	fmt.Printf(">>> looking up |%s| %v %v\n", section, ok, m)
	return m, ok
}

func GetMetadata(metadataFile string) Metadata {
	input, err := ioutil.ReadFile(metadataFile)
	check(err)

	metadata := Metadata{}
	err = yaml.Unmarshal(input, &metadata)
	check(err)

	ruleInfo = make(map[string]RuleMetadata)
	sectionInfo = make(map[string]SectionMetadata)

	for i := range metadata.Sections {
		metadata.Sections[i].Position = i
		sectionInfo[metadata.Sections[i].Section] = metadata.Sections[i]
	}

	for i := range metadata.Rules {
		metadata.Rules[i].Position = i
		ruleInfo[metadata.Rules[i].Rule] = metadata.Rules[i]
	}

	//	spew.Dump(metadata.Sections)
	return metadata
}
