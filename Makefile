serve: grumpy
#	-killall grumpy
	grumpy serve -H 0.0.0.0

grumpy:
	go build -o grumpy main.go
	go install

reload: grumpy
	grumpy load --clear

container:
	docker build -t uber/grumpy .
	docker run -p 8000:8000 uber/grumpy

