package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type Fragment struct {
	gorm.Model
	Position int
	RuleID   uint
	Title    string
	Content  string
}

type Tag struct {
	gorm.Model
	Label string
}

type Rule struct {
	gorm.Model
	Position  int
	SectionID uint `gorm:"index"`
	Anchor    string
	Tag       string
	Title     string
	Fragments []Fragment
	Tags      []Tag `gorm:"many2many:rule_tags;"`
}

type Section struct {
	gorm.Model
	Position int
	Anchor   string
	Tag      string
	Title    string
	Rules    []Rule
}

type Warning struct {
	gorm.Model
	Filename   string
	LineNumber int
	Column     int
	Checker    string
	Message    string
}
