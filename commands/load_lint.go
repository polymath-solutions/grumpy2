package commands

import (
	"github.com/spf13/cobra"
	//	"github.com/spf13/viper"
	"bufio"
	"fmt"
	"github.com/jinzhu/gorm"
	"os"
	"regexp"
	"strconv"
	"strings"
	"uber.com/grumpy/models"
)

var (
	loadLintCmd = &cobra.Command{
		Use:   "load-lint",
		Short: "Initialize database with lint errors",
		Long:  ``,

		Run: loadLint,
	}
)

func init() {
	//	loadCmd.Flags().StringVarP(&database, "database", "d", GrumpyCmd.PersistentFlags().Lookup("database").DefValue, "The database")
	loadLintCmd.Flags().BoolVarP(&clear, "clear", "", false, "clear")
}

func clearLintTable(database string) {
	db, err := gorm.Open("sqlite3", database)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	db.DropTableIfExists(&models.Warning{})
}

var warningRe = regexp.MustCompile(`^([^:]*):([0-9]+):([0-9]+): warning: ([^[]+)\[(.*)\]$`)

func loadLint(ccmd *cobra.Command, args []string) {
	if clear {
		clearLintTable(database)
	}

	db, err := gorm.Open("sqlite3", database)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	db.AutoMigrate(&models.Warning{})

	f, err := os.Open("tidy_warnings.txt")
	check(err)
	fs := bufio.NewScanner(f)

	inCheckerList := false
	var checkers []string

	for fs.Scan() {
		line := fs.Text()
		if line == "Enabled checks:" {
			inCheckerList = true
		}
		if inCheckerList && strings.HasPrefix(line, "    ") {
			checkers = append(checkers, strings.TrimPrefix(line, "    "))
		}
		if inCheckerList && line == "" {
			inCheckerList = false
		}

		if inCheckerList == false {
			if m := warningRe.FindAllStringSubmatch(line, -1); m != nil {
				filename := m[0][1]
				lineNumber, _ := strconv.Atoi(m[0][2])
				column, _ := strconv.Atoi(m[0][3])
				message := m[0][4]
				checkerName := m[0][5]

				if strings.HasPrefix(filename, "bazel-out") {
					continue
				}

				warning := models.Warning{
					Filename:   filename,
					LineNumber: lineNumber,
					Column:     column,
					Message:    message,
					Checker:    checkerName,
				}

				db.Create(&warning)
				//				fmt.Printf("[%s] %s (%s,%s): %s\n", checker, filename, line, column, message)
			}
		}

	}
	fmt.Println("ingesting warnings for AV")
}
