package commands

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/blevesearch/bleve"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/pksunkara/pygments"
	"github.com/spf13/cobra"
	"github.com/thoas/stats"
	blackfriday "gopkg.in/russross/blackfriday.v2"
	"uber.com/grumpy/models"
	"uber.com/grumpy/utils"
)

var (
	serveCmd = &cobra.Command{
		Use:   "serve",
		Short: "Run a grumpy server",
		Long:  ``,
		Run:   serve,
	}
)

func init() {
	// TODO(mav) there's duplication between what's in here and what we declare in commands.go
	serveCmd.Flags().StringVarP(&database, "database", "d", GrumpyCmd.PersistentFlags().Lookup("database").DefValue, "The database")
	serveCmd.Flags().StringVarP(&host, "host", "H", GrumpyCmd.PersistentFlags().Lookup("host").DefValue, "IP address")
	serveCmd.Flags().StringVarP(&port, "port", "p", GrumpyCmd.PersistentFlags().Lookup("port").DefValue, "Port")
}

// HandleErrorJson wraps error in JSON structure.
func HandleErrorJson(w http.ResponseWriter, err error) {
	var errMap map[string]string

	if err == nil {
		errMap = map[string]string{"Error": "Error struct is nil."}
	} else {
		errMap = map[string]string{"Error": err.Error()}
	}

	errJson, _ := json.Marshal(errMap)
	http.Error(w, string(errJson), http.StatusInternalServerError)
}

type grumpyContext struct {
	DB    *gorm.DB
	Index *bleve.Index
}

type grumpyHandler struct {
	context *grumpyContext
	handler func(*grumpyContext, http.ResponseWriter, *http.Request) (int, error)
}

func (gh grumpyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Updated to pass gh.appContext as a parameter to our handler type.
	status, err := gh.handler(gh.context, w, r)
	if err != nil {
		log.Printf("HTTP %d: %q", status, err)
		switch status {
		case http.StatusNotFound:
			http.NotFound(w, r)
			// And if we wanted a friendlier error page, we can
			// now leverage our context instance - e.g.
			// err := gh.renderTemplate(w, "http_404.tmpl", nil)
		case http.StatusInternalServerError:
			http.Error(w, http.StatusText(status), status)
		default:
			http.Error(w, http.StatusText(status), status)
		}
	}
}

func serve(ccmd *cobra.Command, args []string) {
	middleStats := stats.New()

	if !utils.FileExists(database) {
		panic(fmt.Sprintf("Cannot find database %s (see grumpy load)", database))
	}

	db, err := gorm.Open("sqlite3", database)
	if err != nil {
		panic(fmt.Sprintf("failed to connect database %s", database))
	}
	defer db.Close()

	db.AutoMigrate(&models.Rule{}, &models.Section{}, &models.Fragment{})

	bleveIndex := "grumpy.bleve"
	index, err := bleve.Open(bleveIndex)
	if err != nil {
		mapping := bleve.NewIndexMapping()
		index, err = bleve.New(bleveIndex, mapping)
		if err != nil {
			panic(err)
		}
	}

	context := &grumpyContext{DB: db, Index: &index}

	r := mux.NewRouter()
	r.Handle("/", grumpyHandler{context, homeHandler})
	r.HandleFunc("/panic", panicHandler)
	r.Handle("/test", grumpyHandler{context, testHandler})
	r.Handle("/rules", grumpyHandler{context, ruleIndex})
	r.Handle("/metadata", grumpyHandler{context, metadataShow})
	r.Handle("/warnings", grumpyHandler{context, warningIndex})
	r.Handle("/warnings-by-file", grumpyHandler{context, warningFileIndex})
	r.Handle("/warnings-by-file/{file:.*}", grumpyHandler{context, warningFileShow})
	r.Handle("/tags", grumpyHandler{context, tagIndex})
	r.Handle("/rules/{tag}", grumpyHandler{context, ruleTagIndex})
	r.Handle("/rule/{ruleId}", grumpyHandler{context, ruleShow})
	r.Handle("/query", grumpyHandler{context, queryHandler}).Queries("q", "{query}")
	r.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Loading favicon")
		http.ServeFile(w, r, "assets/images/favicon.ico")
	})
	r.HandleFunc("/stats", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		stats := middleStats.Data()
		b, _ := json.Marshal(stats)
		w.Write(b)
	})

	// Path of static files must be last!
	// TODO(mav) once we have webpack in place, stuff in assets will be compiled into public
	// and that will be were we serve assets from.
	//r.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("assets/"))))
	r.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("./public/"))))
	n := negroni.New()
	recovery := negroni.NewRecovery()
	//recovery.SetFormatter(&negroni.HTMLPanicFormatter{})
	recovery.Formatter = &negroni.HTMLPanicFormatter{}
	n.Use(recovery)
	n.Use(middleStats)
	n.UseHandler(r)
	n.Run(fmt.Sprintf("%s:%s", host, port))
}

func homeHandler(context *grumpyContext, w http.ResponseWriter, r *http.Request) (int, error) {
	w.Header().Set("Content-type", "text/html")
	db := context.DB

	var ruleCount int
	db.Debug().Table("rules").Count(&ruleCount)

	tmpl, err := template.ParseFiles(
		"templates/grumpy.html.tmpl",
		"templates/home.html.tmpl",
		"templates/navbar.html.tmpl")
	if err != nil {
		HandleErrorJson(w, err)
		return 1, err
	}

	err = tmpl.Execute(w, map[string]interface{}{
		"ruleCount": ruleCount,
	})
	if err != nil {
		HandleErrorJson(w, err)
		return 1, err
	}

	// fmt.Fprintf(w, "<hr/>")
	// fmt.Fprintf(w, "\n<p><a href='/tags'>Tags</a>&emsp;&emsp;\n")
	// fmt.Fprintf(w, "\n<a href='/warnings'>Warnings (by checker)</a>&emsp;&emsp;\n")
	// fmt.Fprintf(w, "\n<a href='/warnings-by-file'>Warnings (by file)</a>&emsp;&emsp;\n")
	// fmt.Fprintf(w, "\n<a href='/rules'>All rules</a>&emsp;&emsp;\n")
	// fmt.Fprintf(w, "\n<a href='/rules/boring'>Boring rules</a>&emsp;&emsp;\n")
	// fmt.Fprintf(w, "\n<a href='/rules/incomplete'>Rules needing work</a>&emsp;&emsp;\n")
	// fmt.Fprintf(w, "\n<a href='/rules/-boring'>Interesting (or at least not boring) rules</a>&emsp;&emsp;\n")
	// fmt.Fprintf(w, "\n<a href='/metadata'>Metadata</a>&emsp;&emsp;\n")
	// fmt.Fprintf(w, "\n<a href='/stats'>Stats</a></p>\n")

	// fmt.Fprintf(w, "<hr/>")
	// fmt.Fprintf(w, "\n<p><a href=\"https://docs.google.com/document/d/18xAlf17tjrLFxYaGapF0lShj4xeevPMkT9VfU_0MAto\">ATG code guidelines RFC</a></p>\n")
	// fmt.Fprintf(w, "\n<p><a href=\"https://docs.google.com/spreadsheets/d/1vv2WKowZECWU6bn4E6uPNZhyGNlmxrNMaN5BzAahbEc\">ATG code guidelines vs C++ Core guidelines</a></p>\n")

	// fmt.Fprintf(w, "\n<p><a href='http://docs.int.uberatc.com/standards/CppStandard.html'>Current ATG rules</a></p>\n")
	// fmt.Fprintf(w, "\n<p><a href='http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines'>Pristine C++ Core guidelines</a></p>\n")

	return 0, nil
}

func panicHandler(w http.ResponseWriter, r *http.Request) {
	panic("Don't panic!")
}

func renderTemplates(w http.ResponseWriter,
	vars map[string]interface{}, templates ...string) (int, error) {
	global_templates := []string{"templates/grumpy.html.tmpl", "templates/navbar.html.tmpl"}
	tmpl, err := template.ParseFiles(append(global_templates, templates...)...)
	if err != nil {
		HandleErrorJson(w, err)
		return 1, err
	}

	err = tmpl.Execute(w, vars)
	if err != nil {
		fmt.Println("template error")
		HandleErrorJson(w, err)
		return 1, err
	}

	return 0, nil
}

func searchRules(db *gorm.DB, index bleve.Index, searchPhrase string) []TemplateRule {
	query := bleve.NewQueryStringQuery(searchPhrase)
	searchRequest := bleve.NewSearchRequest(query)

	searchRequest.Size = 50
	searchResult, err := index.Search(searchRequest)
	if err != nil {
		//	 	HandleErrorJson(w, err)
		return nil
	}

	var rules []TemplateRule
	for _, result := range searchResult.Hits {
		if result.Score < .6 {
			return rules
		}
		title := ""
		rule := models.Rule{}
		if err := db.Where(&models.Rule{Tag: result.ID}).First(&rule).Error; err == nil {
			title = rule.Title
		}

		rules = append(rules, TemplateRule{
			Tag:   result.ID,
			Title: title,
			Score: fmt.Sprintf("%.2f", result.Score),
		})
	}
	return rules
	// fmt.Fprintf(w, "<h1>Search, %q</h1>\n", html.EscapeString(searchPhrase))
	// fmt.Fprintf(w, "<p><a href='/'>Home</a></p>")
	// //	query := bleve.NewMatchPhraseQuery(searchPhrase)
	// query := bleve.NewQueryStringQuery(searchPhrase)
	// searchRequest := bleve.NewSearchRequest(query)

	// searchRequest.Size = 50
	// //searchRequest.IncludeLocations = true
	// //	searchRequest.Highlight = bleve.NewHighlight()

	// index := *context.Index
	// searchResult, err := index.Search(searchRequest)
	// if err != nil {
	// 	HandleErrorJson(w, err)
	// 	return 1, err
	// }

	// db := context.DB

	// aboveTreshold := true
	// treshold := 0.6
	// belowTreshold := 0
	// somethingAboveTreshold := false

	// fmt.Fprintf(w, "\n<ul>\n")
	// for _, result := range searchResult.Hits {
	// 	//url := "<a href=\"rule/" + result.ID + "\">" + result.ID + "</a>"
	// 	var rule models.Rule
	// 	title := ""
	// 	if err := db.Where(&models.Rule{Tag: result.ID}).First(&rule).Error; err == nil {
	// 		title = rule.Title
	// 	}
	// 	if aboveTreshold && result.Score < treshold {
	// 		aboveTreshold = false
	// 		if !somethingAboveTreshold {
	// 			fmt.Fprintf(w, "<p> Nothing interesting</p>")
	// 		}
	// 		fmt.Fprintf(w, "<hr/>\n")
	// 		//			belowTreshold = 1
	// 	}
	// 	if aboveTreshold || (!aboveTreshold && belowTreshold < 10) {
	// 		if aboveTreshold {
	// 			somethingAboveTreshold = true
	// 		}
	// 		fmt.Fprintf(w, "<li><a href=\"/rule/%s\">%s</a> [%f] %s</li>\n", result.ID, result.ID, result.Score, title)
	// 		spew.Dump(result.Fragments)
	// 		for k, n := range result.Fragments["name"] {
	// 			fmt.Fprintf(w, "%d : [%v]\n", k, n)
	// 		}
	// 		if !aboveTreshold {
	// 			belowTreshold += 1
	// 		}
	// 	}
	// }

	return nil
}

func queryHandler(context *grumpyContext, w http.ResponseWriter, r *http.Request) (int, error) {
	w.Header().Set("Content-type", "text/html")

	contextVars := mux.Vars(r)
	searchPhrase := contextVars["query"]

	vars := map[string]interface{}{
		"searchPhrase": searchPhrase,
		"rules":        searchRules(context.DB, *context.Index, searchPhrase),
	}
	return renderTemplates(w, vars, "templates/rule_search.html.tmpl")

	// fmt.Fprintf(w, "<h1>Search, %q</h1>\n", html.EscapeString(searchPhrase))
	// fmt.Fprintf(w, "<p><a href='/'>Home</a></p>")
	// //	query := bleve.NewMatchPhraseQuery(searchPhrase)
	// query := bleve.NewQueryStringQuery(searchPhrase)
	// searchRequest := bleve.NewSearchRequest(query)

	// searchRequest.Size = 50
	// //searchRequest.IncludeLocations = true
	// //	searchRequest.Highlight = bleve.NewHighlight()

	// index := *context.Index
	// searchResult, err := index.Search(searchRequest)
	// if err != nil {
	// 	HandleErrorJson(w, err)
	// 	return 1, err
	// }

	// db := context.DB

	// aboveTreshold := true
	// treshold := 0.6
	// belowTreshold := 0
	// somethingAboveTreshold := false

	// fmt.Fprintf(w, "\n<ul>\n")
	// for _, result := range searchResult.Hits {
	// 	//url := "<a href=\"rule/" + result.ID + "\">" + result.ID + "</a>"
	// 	var rule models.Rule
	// 	title := ""
	// 	if err := db.Where(&models.Rule{Tag: result.ID}).First(&rule).Error; err == nil {
	// 		title = rule.Title
	// 	}
	// 	if aboveTreshold && result.Score < treshold {
	// 		aboveTreshold = false
	// 		if !somethingAboveTreshold {
	// 			fmt.Fprintf(w, "<p> Nothing interesting</p>")
	// 		}
	// 		fmt.Fprintf(w, "<hr/>\n")
	// 		//			belowTreshold = 1
	// 	}
	// 	if aboveTreshold || (!aboveTreshold && belowTreshold < 10) {
	// 		if aboveTreshold {
	// 			somethingAboveTreshold = true
	// 		}
	// 		fmt.Fprintf(w, "<li><a href=\"/rule/%s\">%s</a> [%f] %s</li>\n", result.ID, result.ID, result.Score, title)
	// 		spew.Dump(result.Fragments)
	// 		for k, n := range result.Fragments["name"] {
	// 			fmt.Fprintf(w, "%d : [%v]\n", k, n)
	// 		}
	// 		if !aboveTreshold {
	// 			belowTreshold += 1
	// 		}
	// 	}
	// }
	// fmt.Fprintf(w, "\n</ul>\n")

	return 0, nil
}

func markdownRender(content []byte, inline bool) string {
	result := string(blackfriday.Run(bytes.Replace(content, []byte("???"), []byte("<span style=\"color:red\">???</span>"), -1)))
	if inline {
		return result[3 : len(result)-5]
	}

	return result
}

func tagIndex(context *grumpyContext, w http.ResponseWriter, r *http.Request) (int, error) {
	w.Header().Set("Content-type", "text/html")
	db := context.DB

	var tags []models.Tag
	db.Table("tags").Find(&tags)

	fmt.Fprintf(w, "<h1>Tags</h1>\n")
	fmt.Fprintf(w, "\n<p><a href='/'>Home</a></p>\n")

	fmt.Fprintf(w, "<ul style=\"list-style-type:square\">\n")
	for _, tag := range tags {
		fmt.Fprintf(w, "<li><a href=\"/rules/%s\">%s</a></li>\n", tag.Label, tag.Label)
	}
	fmt.Fprintf(w, "</ul>")
	return 0, nil
}

func ruleIndex(context *grumpyContext, w http.ResponseWriter, r *http.Request) (int, error) {
	w.Header().Set("Content-type", "text/html")
	db := context.DB

	var sections []models.Section
	db.Table("sections").Order("position").Find(&sections)

	var templateSections []TemplateSection
	for _, section := range sections {
		var templateRules []TemplateRule
		var rules []models.Rule
		db.Debug().Model(&section).Order("position").Related(&rules)
		for _, rule := range rules {
			var templateTags []string
			var tags []models.Tag
			db.Debug().Model(&rule).Related(&tags, "Tags")
			for _, tag := range tags {
				templateTags = append(templateTags, tag.Label)
			}
			templateRules = append(templateRules, TemplateRule{
				Title: rule.Title,
				Tag:   rule.Tag,
				Tags:  templateTags,
			})
		}
		templateSections = append(templateSections, TemplateSection{
			Title: section.Title,
			Rules: templateRules,
		})
	}

	tmpl, err := template.ParseFiles("templates/grumpy.html.tmpl", "templates/navbar.html.tmpl", "templates/rule_index.html.tmpl")
	if err != nil {
		HandleErrorJson(w, err)
		return 1, err
	}

	err = tmpl.Execute(w,
		map[string]interface{}{"sections": templateSections})
	if err != nil {
		fmt.Println("template error")
		HandleErrorJson(w, err)
		return 1, err
	}

	// fmt.Fprintf(w, "<h1>Rules Index</h1>\n")
	// fmt.Fprintf(w, "\n<p><a href='/'>Home</a></p>\n")

	// var sections []models.Section
	// db.Table("sections").Order("position").Find(&sections)
	// fmt.Fprintf(w, "<ul style=\"list-style-type:square\">\n")
	// for _, section := range sections {
	// 	var rules []models.Rule
	// 	db.Debug().Model(&section).Order("position").Related(&rules)
	// 	fmt.Fprintf(w, "<li>%s %s</li>\n", section.Tag, markdownRender([]byte(section.Title), true))
	// 	fmt.Fprintf(w, "<ul style=\"list-style-type:none\">\n")
	// 	for _, rule := range rules {
	// 		var tags []models.Tag
	// 		db.Debug().Model(&rule).Related(&tags, "Tags")
	// 		fmt.Fprintf(w, "<li><a href=/rule/%s>%s</a>&emsp;", rule.Tag, markdownRender([]byte(rule.Title), true))
	// 		for _, t := range tags {
	// 			fmt.Fprintf(w, "<small>%s</small>&nbsp;", t.Label)
	// 		}
	// 		fmt.Fprintf(w, "</li>\n")
	// 	}
	// 	fmt.Fprintf(w, "</ul>\n")
	// }
	// fmt.Fprintf(w, "</ul>\n")

	return 0, nil
}

func warningIndex(context *grumpyContext, w http.ResponseWriter, r *http.Request) (int, error) {
	w.Header().Set("Content-type", "text/html")
	db := context.DB

	fmt.Fprintf(w, "<h1>clang-tidy warnings</h1>\n")
	fmt.Fprintf(w, "\n<p><a href='/'>Home</a></p>\n")

	type Result struct {
		Count   int
		Checker string
	}

	var results []Result

	db.Debug().
		Table("warnings").
		Select("count(*) as count, checker as checker").
		Group("checker").
		Order("count desc").
		Scan(&results)

	fmt.Fprintf(w, "<ul style=\"list-style-type:square\">\n")
	for _, result := range results {
		fmt.Fprintf(w, "<li>%s (<a href=\"https://clang.llvm.org/extra/clang-tidy/checks/%s.html\">doc</a>)&emsp;&emsp;%d</li>\n", result.Checker, result.Checker, result.Count)
	}
	fmt.Fprintf(w, "</ul>\n")

	return 0, nil
}

func warningFileIndex(context *grumpyContext, w http.ResponseWriter, r *http.Request) (int, error) {
	w.Header().Set("Content-type", "text/html")
	db := context.DB

	fmt.Fprintf(w, "<h1>clang-tidy warnings</h1>\n")
	fmt.Fprintf(w, "\n<p><a href='/'>Home</a></p>\n")

	type Result struct {
		Count    int
		Filename string
	}

	var results []Result

	db.Debug().
		Table("warnings").
		Select("count(*) as count, filename as filename").
		Group("filename").
		Order("count desc").
		Scan(&results)

	fmt.Fprintf(w, "<ul style=\"list-style-type:square\">\n")
	for _, result := range results {
		fmt.Fprintf(w, "<li><a href=\"/warnings-by-file/%s\">%s</a>&emsp;&emsp;%d</li>\n", url.PathEscape(result.Filename), result.Filename, result.Count)
	}
	fmt.Fprintf(w, "</ul>\n")

	return 0, nil
}

func warningFileShow(context *grumpyContext, w http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	file, _ := url.PathUnescape(vars["file"])

	fmt.Println(">>> FILE:", file)
	w.Header().Set("Content-type", "text/html")
	db := context.DB

	var warnings []models.Warning

	db.Debug().Table("warnings").Where("warnings.filename=?", file).Find(&warnings)

	fmt.Fprintf(w, "<ul style=\"list-style-type:square\">\n")
	for _, warning := range warnings {
		fmt.Fprintf(w, "<li>Line: %d, %s &emsp;&emsp;&emsp;&emsp;[%s]</li>\n", warning.LineNumber, warning.Message, warning.Checker)

	}
	fmt.Fprintf(w, "</ul>\n")

	fmt.Fprintf(w, "<h1>Warnings for file %s</h1>\n", file)
	fmt.Fprintf(w, "\n<p><a href='/'>Home</a></p>\n")

	return 0, nil
}

type TemplateRule struct {
	Title string
	Tag   string
	Score string
	Tags  []string
}

type TemplateSection struct {
	Title string
	Rules []TemplateRule
}

func ruleTagIndex(context *grumpyContext, w http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	tag := vars["tag"]
	negateTag := false

	w.Header().Set("Content-type", "text/html")
	db := context.DB

	var rules []models.Rule
	if negateTag {
		db.Debug().Joins("LEFT JOIN rule_tags ON rule_tags.rule_id=rules.id").Joins("LEFT JOIN tags ON rule_tags.tag_id=tags.id").Where("tags.label!=?", tag).Or("tags.label IS NULL").Find(&rules)
	} else {
		db.Debug().Joins("JOIN rule_tags ON rule_tags.rule_id=rules.id").Joins("JOIN tags ON rule_tags.tag_id=tags.id").Where("tags.label=?", tag).Find(&rules)
	}

	tmpl, err := template.ParseFiles("templates/grumpy.html.tmpl", "templates/navbar.html.tmpl", "templates/rule_tag_index.html.tmpl")
	if err != nil {
		HandleErrorJson(w, err)
		return 1, err
	}

	err = tmpl.Execute(w,
		map[string]interface{}{"rules": rules, "tag": tag, "negateTag": negateTag})
	if err != nil {
		fmt.Println("template error")
		HandleErrorJson(w, err)
		return 1, err
	}

	// fmt.Fprintf(w, "<h1>Rules Index for tag %s</h1>\n", tag)
	// fmt.Fprintf(w, "\n<p><a href='/'>Home</a></p>\n")

	// if strings.HasPrefix(tag, "-") {
	// 	negateTag = true
	// 	tag = strings.TrimPrefix(tag, "-")
	// }

	// var rules []models.Rule
	// if negateTag {
	// 	db.Debug().Joins("LEFT JOIN rule_tags ON rule_tags.rule_id=rules.id").Joins("LEFT JOIN tags ON rule_tags.tag_id=tags.id").Where("tags.label!=?", tag).Or("tags.label IS NULL").Find(&rules)
	// } else {
	// 	db.Debug().Joins("JOIN rule_tags ON rule_tags.rule_id=rules.id").Joins("JOIN tags ON rule_tags.tag_id=tags.id").Where("tags.label=?", tag).Find(&rules)
	// }

	// fmt.Fprintf(w, "<ul style=\"list-style-type:none\">\n")
	// for _, rule := range rules {
	// 	fmt.Fprintf(w, "<li><a href=/rule/%s>%s</a></li>", rule.Tag, markdownRender([]byte(rule.Title), true))
	// 	fmt.Fprintf(w, "\n")
	// }
	// fmt.Fprintf(w, "</ul>\n")

	// // var sections []models.Section
	// // db.Table("sections").Order("position").Find(&sections)
	// // fmt.Fprintf(w, "<ul style=\"list-style-type:square\">\n")
	// // for _, section := range sections {
	// // 	var rules []models.Rule
	// // 	db.Debug().Model(&section).Order("position").Related(&rules)
	// // 	fmt.Fprintf(w, "<li>%s %s</li>\n", section.Tag, markdownRender([]byte(section.Title), true))
	// // 	fmt.Fprintf(w, "<ul style=\"list-style-type:none\">\n")
	// // 	for _, rule := range rules {
	// // 		var tags []models.Tag
	// // 		db.Debug().Model(&rule).Related(&tags, "Tags")
	// // 		fmt.Fprintf(w, "<li><a href=/rule/%s>%s</a></li>", rule.Tag, markdownRender([]byte(rule.Title), true))
	// // 		for _, t := range tags {
	// // 			fmt.Fprintf(w, "%s ", t.Label)
	// // 		}
	// // 		fmt.Fprintf(w, "\n")
	// // 	}
	// // 	fmt.Fprintf(w, "</ul>\n")
	// // }
	// // fmt.Fprintf(w, "</ul>\n")

	return 0, nil
}

func ruleShow(context *grumpyContext, w http.ResponseWriter, r *http.Request) (int, error) {
	vars := mux.Vars(r)
	ruleID := vars["ruleId"]

	w.Header().Set("Content-type", "text/html")
	db := context.DB

	tmpl, err := template.ParseFiles("templates/grumpy.html.tmpl", "templates/navbar.html.tmpl", "templates/rule.html.tmpl")
	//	tmpl := template.Must(template.New("rule").Funcs(funcMap).‌​ParseFiles("templates/grumpy.html.tmpl", "templates/rule.html.tmpl")‌​)
	//tmpl, err := template.ParseFiles("templates/grumpy.html.tmpl", "templates/rule.html.tmpl")
	if err != nil {
		HandleErrorJson(w, err)
		return 1, err
	}

	var rule models.Rule
	if err := db.Where(&models.Rule{Tag: ruleID}).First(&rule).Error; err != nil {
		HandleErrorJson(w, err)
		return 1, err
	} else {
		var fragments []models.Fragment
		db.Model(&rule).Related(&fragments)
		type TemplateFragment struct {
			Type    string
			Title   string //template.HTML
			Content template.HTML
		}

		var templateFragments []TemplateFragment

		for _, fragment := range fragments {
			if fragment.Title == "Example" {
				scanner := bufio.NewScanner(strings.NewReader(fragment.Content))
				for scanner.Scan() {
					fmt.Printf("|%s|\n", scanner.Text())
				}
				templateFragments = append(templateFragments, TemplateFragment{
					Title:   fragment.Title,
					Content: template.HTML(pygments.Highlight(fragment.Content, "c++", "html", "utf-8")),
				})
			} else {
				templateFragments = append(templateFragments, TemplateFragment{
					Type:    "paragraph",
					Title:   fragment.Title,
					Content: template.HTML(markdownRender([]byte(fragment.Content), false)),
				})
			}
		}
		err = tmpl.Execute(w,
			map[string]interface{}{
				"title":     template.HTML(markdownRender([]byte(rule.Title), true)),
				"fragments": templateFragments})
		if err != nil {
			fmt.Println("template error")
			HandleErrorJson(w, err)
			return 1, err
		}

	}

	// fmt.Fprintf(w, "<h1>Rule %s</h1>\n<a href=\"/rules\">Rule list</a>", ruleID)

	// fmt.Fprintf(w, "\n<p><a href='/'>Home</a></p>\n")

	// var rule models.Rule
	// if err := db.Where(&models.Rule{Tag: ruleID}).First(&rule).Error; err != nil {
	// 	fmt.Fprintln(w, "ERROR!")
	// } else {
	// 	fmt.Fprintf(w, "<h1>%s</h1>\n", markdownRender([]byte(rule.Title), true))
	// 	var fragments []models.Fragment
	// 	db.Model(&rule).Related(&fragments)
	// 	for _, fragment := range fragments {
	// 		fmt.Fprintf(w, "<h5>%s</h5>", fragment.Title)
	// 		fmt.Println(">>>>>>>>>", fragment.Title)
	// 		if fragment.Title == "Example" {
	// 			fmt.Println(">>>>>>>>>")
	// 			fmt.Fprintf(w, pygments.Highlight(fragment.Content, "c++", "html", "utf-8"))
	// 		} else {
	// 			fmt.Fprintf(w, markdownRender([]byte(fragment.Content), false))
	// 		}
	// 	}
	// }

	return 0, nil
}

func metadataShow(context *grumpyContext, w http.ResponseWriter, r *http.Request) (int, error) {
	w.Header().Set("Content-type", "text/html")

	fmt.Fprintf(w, "<h1>Metadata</h1>")
	fmt.Fprintf(w, "\n<p><a href='/'>Home</a></p>\n")

	input, err := ioutil.ReadFile("metadata.yaml")
	check(err)

	fmt.Fprintf(w, "<p><pre><small>%s</small></pre></p>", string(input))
	return 0, nil
}

func testHandler(context *grumpyContext, w http.ResponseWriter, r *http.Request) (int, error) {
	w.Header().Set("Content-type", "text/html")

	tmpl, err := template.ParseFiles("templates/grumpy.html.tmpl", "templates/test.html.tmpl")
	if err != nil {
		HandleErrorJson(w, err)
		return 1, err
	}

	tmpl.Execute(w, nil)

	return 0, nil
}
