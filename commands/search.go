package commands

import (
	"fmt"

	"github.com/blevesearch/bleve"
	"github.com/spf13/cobra"
	//	"github.com/spf13/viper"
	// "github.com/gorilla/mux"
	// "github.com/jinzhu/gorm"
	// _ "github.com/jinzhu/gorm/dialects/sqlite"
	// "html"
	// "log"
	// "net/http"
	// //	"time"
	// "encoding/json"
	// "github.com/codegangsta/negroni"
	// "github.com/thoas/stats"
	// blackfriday "gopkg.in/russross/blackfriday.v2"
	// "uber.com/grumpy/models"
	// "uber.com/grumpy/utils"
	"github.com/davecgh/go-spew/spew"
)

var (
	searchCmd = &cobra.Command{
		Use:   "search",
		Short: "search rule database",
		Long:  ``,

		Run: search,
	}
)

func search(ccmd *cobra.Command, args []string) {
	if args == nil {
		return
	}
	searchPhrase := args[0]
	fmt.Printf("Serching for |%s|\n", searchPhrase)

	index, _ := bleve.Open("grumpy.bleve")
	//	query := bleve.NewMatchPhraseQuery(searchPhrase)
	query := bleve.NewQueryStringQuery(searchPhrase)
	searchRequest := bleve.NewSearchRequest(query)
	searchResult, _ := index.Search(searchRequest)
	spew.Dump(searchResult)
}
