package commands

import (
	"fmt"
	//	"github.com/davecgh/go-spew/spew"
	"github.com/spf13/cobra"
	blackfriday "gopkg.in/russross/blackfriday.v2"
	"io/ioutil"
)

var (
	//	database string

	debugCmd = &cobra.Command{
		Use:   "debug",
		Short: "Temporary debug stuff",
		Long:  ``,

		Run: debug,
	}
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func renderChildren(node *blackfriday.Node) string {
	content := ""

	for c := node.FirstChild; c != nil; c = c.Next {
		c.Walk(func(node *blackfriday.Node, entering bool) blackfriday.WalkStatus {
			content += string(c.Literal)
			return blackfriday.SkipChildren
		})
	}

	// node.FirstChild.Walk(func(node *blackfriday.Node, entering bool) blackfriday.WalkStatus {
	// 	return renderer.renderNode(nil, node, entering)
	// })

	return content
}

func debug(ccmd *cobra.Command, args []string) {
	if len(args) == 0 {
		return
	}
	file := args[0]
	input, err := ioutil.ReadFile(file)
	check(err)

	//	renderer := ruleRenderer{}
	fmt.Println(string(input))

	ast := blackfriday.New().Parse(input)
	ast.Walk(func(node *blackfriday.Node, entering bool) blackfriday.WalkStatus {
		extra := ""
		if node.Type == blackfriday.Link {
			children := renderChildren(node)
			extra = fmt.Sprintf("[%s](%s)", children, node.LinkData.Destination)
		}
		if entering {
			fmt.Printf("Node %s\t |%s| %s\n", node.Type.String(), node.Literal, extra)
		} else {
			fmt.Println("---------------")
		}
		if extra == "" {
			return blackfriday.GoToNext
		} else {
			return blackfriday.SkipChildren
		}
	})

	//	html := blackfriday.Run(input)
	//	fmt.Println(string(html))

}
