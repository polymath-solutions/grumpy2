package commands

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	//	config   string //
	//	daemon   bool   //
	//	version  bool   //
	database string
	host     string
	port     string

	// GrumpyCmd ...
	GrumpyCmd = &cobra.Command{
		Use:   "",
		Short: "",
		Long:  ``,

		Run: func(ccmd *cobra.Command, args []string) {
			// fall back on default help if no args/flags are passed
			ccmd.HelpFunc()(ccmd, args)
		},
	}
)

func init() {

	GrumpyCmd.GenBashCompletionFile("grumpy_autocomplete.bash")

	// set config defaults
	viper.SetDefault("garbage-collect", false)

	// persistent flags
	GrumpyCmd.PersistentFlags().StringP("database", "d", "grumpy.db", "sqlite3 database")
	GrumpyCmd.PersistentFlags().StringP("metadata", "m", "metadata.yaml", "metadata complementing input during load operations")
	GrumpyCmd.PersistentFlags().StringP("host", "H", "127.0.0.1", "hostname/IP the server listens to")
	GrumpyCmd.PersistentFlags().StringP("port", "p", "8000", "port")

	//	GrumpyCmd.PersistentFlags().BoolP("clear", "", false, "clear database")

	viper.BindPFlag("database", GrumpyCmd.PersistentFlags().Lookup("database"))
	viper.BindPFlag("metadata", GrumpyCmd.PersistentFlags().Lookup("metadata"))
	viper.BindPFlag("host", GrumpyCmd.PersistentFlags().Lookup("host"))
	viper.BindPFlag("port", GrumpyCmd.PersistentFlags().Lookup("port"))

	//	GrumpyCmd.AddCommand(showCmd)
	GrumpyCmd.AddCommand(serveCmd)
	GrumpyCmd.AddCommand(loadCmd)
	GrumpyCmd.AddCommand(loadLintCmd)
	//	GrumpyCmd.AddCommand(searchCmd)
	//	GrumpyCmd.AddCommand(debugCmd)

}
