package commands

import (
	"fmt"

	"github.com/spf13/cobra"
	//	"github.com/spf13/viper"
)

var (

	// alias for show
	fetchCmd = &cobra.Command{
		Hidden: true,

		Use:   "fetch",
		Short: "Display a file from the hoarder storage",
		Long:  ``,

		Run: show,
	}

	// alias for show
	getCmd = &cobra.Command{
		Hidden: true,

		Use:   "get",
		Short: "Display a file from the hoarder storage",
		Long:  ``,

		Run: show,
	}

	//
	showCmd = &cobra.Command{
		Use:   "show",
		Short: "Display a file from the hoarder storage",
		Long:  ``,

		Run: show,
	}
)

// init
func init() {
	//	fetchCmd.Flags().StringVarP(&key, "key", "k", "", "The key to get the data by")
	//	getCmd.Flags().StringVarP(&key, "key", "k", "", "The key to get the data by")
	//	showCmd.Flags().StringVarP(&key, "key", "k", "", "The key to get the data by")
}

// show utilizes the api to show data associated to key
func show(ccmd *cobra.Command, args []string) {

	fmt.Println("SHOW")
	// handle any missing args
	//	switch {
	//	case key == "":
	//		fmt.Println("Missing key - please provide the key for the record you'd like to create")
	//		return
	//	}

	// do command stuff...
}
