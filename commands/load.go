package commands

import (
	"github.com/spf13/cobra"
	//	"github.com/spf13/viper"
	"uber.com/grumpy/ingest"
)

var (
	//	database string
	metadata string

	clear bool

	loadCmd = &cobra.Command{
		Use:   "load",
		Short: "Initialize database",
		Long:  ``,

		Run: load,
	}
)

func init() {
	loadCmd.Flags().StringVarP(&database, "database", "d", GrumpyCmd.PersistentFlags().Lookup("database").DefValue, "The database")
	loadCmd.Flags().StringVarP(&metadata, "metadata", "m", GrumpyCmd.PersistentFlags().Lookup("metadata").DefValue, "configuration")

	loadCmd.Flags().BoolVarP(&clear, "clear", "", false, "clear")

}

func load(ccmd *cobra.Command, args []string) {
	if clear {
		ingest.ClearDatabase(database)
	}
	ingest.RulesIngest("TEST.md", database, metadata)
}
